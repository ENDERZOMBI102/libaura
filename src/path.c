//
// Created by Aurora on 12/05/2023.
//

#include <string.h>

#include "libaura/decorator.h"

#include "libaura/path.h"


const char* str_filename( const char* path ) {
	auto size = strlen( path );

	while ( path[size] != '/' )
		size--;

	return path + size + 1;
}
