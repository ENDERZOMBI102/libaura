//
// Created by Aurora on 21/04/2023.
//
#include <stdio.h>
#include <string.h>

#include "libaura/memory.h"
#include "libaura/result.h"

#include "libaura/linkedlist.h"

struct Node { void* value; struct Node* next; struct Node* prev; };


Result(void*, LinkedListError_t) ll_get( LinkedList_t* this, size_t index ) {
	static char err_message[] = "Index `%d` too large for size `%d`";
	// get that higher index outta here!
	if ( this->size && index > this->size ) {
		char res[ sizeof( err_message ) + 12 ] = { 0 };
		sprintf( res, err_message, index, this->size );
		return ERR( LLE_INDEX_OUT_OF_BOUNDS, strdup( res ) );
	}
	// special case the first and last element
	if ( index == 0 )
		return OK( this->node->value );
	if ( index == this->size )
		return OK( this->last->value );

	// find the requested element
	struct Node* node;
	auto size = this->size;
	if ( index > size / 2 ) {
		node = this->last;
		for ( size_t i = size - 1; i != index; i-- )
			node = node->prev;
	} else {
		node = this->node;
		for ( size_t i = 0; i < size && i < index - 1; i++ )
			node = node->next;
	}
	return OK( node->value );
}

LinkedListError_t ll_push( LinkedList_t* this, void* value ) {
	TRY_MALLOC( struct Node, node, return LLE_ALLOC_FAILED, "Failed to alloc new Node for linked list." );
	node->value = value;
	node->next = NULL;
	node->prev = NULL;

	if (! this->last )
		this->node = node;
	else {
		node->prev = this->last;
		this->last->next = node;
	}

	this->last = node;
	this->size += 1;
	return LLE_NO_ERROR;
}


