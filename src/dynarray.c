//
// Created by Aurora on 14/04/2023.
//

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "libaura/iter.h"
#include "libaura/sort.h"

#include "libaura/dynarray.h"

DynArrayError_t da_new( DynArray_t* out, size_t initialSize ) {
	// init the dynarray and its memory buffer
	out->used = 0;
	out->size = 0;
	out->memory = calloc( initialSize, sizeof( void* ) );

	// error if we didn't a successful allocation
	if ( out->memory == NULL )
		return DAE_ALLOC_FAILED;

	// set the actual size _only_ if we managed to allocate
	out->size = initialSize;

	// no errors
	return DAE_NO_ERROR;
}

DynArrayError_t da_get( const DynArray_t* this, size_t index, void** out ) {
	if (! this->size )  // is the array valid?
		return DAE_INVALID_ARRAY;

	// if the requested index is outside the range of the used slots, error out
	if ( this->used - 1 < index )
		return DAE_INDEX_OUT_OF_BOUNDS;

	*out = this->memory[ index ];
	return DAE_NO_ERROR;
}

bool da_iter( const DynArray_t* this, IterCallback_t callback, void* user ) {
	if (! this->size )  // is the array valid?
		return false;

	size_t index = 0;
	void* value = NULL;
	while ( index < this->used ) {
		da_get( this, index, &value );
		// if asked to stop, return directly saying that we ended before we reached the end of the array
		if ( ( *callback )( index, (const void**) &value, user ) == ITER_BREAK )
			return true;
		index += 1;
	}
	return false;
}

DynArrayError_t da_set( DynArray_t* this, size_t index, void* value ) {
	if (! this->size )  // is the array valid?
		return DAE_INVALID_ARRAY;

	// must be inside the range of used slots
	if ( this->used < index )
		return DAE_INDEX_OUT_OF_BOUNDS;

	this->memory[ index ] = value;

	return DAE_NO_ERROR;
}

DynArrayError_t da_remove( DynArray_t* this, size_t index, void** out ) {
	if (! this->size )  // is the array valid?
		return DAE_INVALID_ARRAY;

	// must be inside the range of used slots
	if ( this->used < index )
		return DAE_INDEX_OUT_OF_BOUNDS;

	// if we were given a pointer, set its value to the slot's value, transferring ownership
	if ( out != NULL )
		*out = this->memory[ index ];

	// move the values after the index by -1
	while ( index + 1 < this->used ) {
		void* value = this->memory[ index + 1 ];
		this->memory[ index ] = value;
		index += 1;
	}

	this->used -= 1;
	return DAE_NO_ERROR;
}

DynArrayError_t da_push( DynArray_t* this, void* value ) {
	if (! this->size )  // is the array valid?
		return DAE_INVALID_ARRAY;

	// do we need to reallocate the array?
	if ( this->size <= this->used ) {
		// yes, realloc the array with a new size
		size_t size = ceil( (double) this->size * 1.5 );

		void* ptr = reallocarray( this->memory, size, sizeof( void* ) );
		if ( !ptr )
			return DAE_ALLOC_FAILED;

		this->memory = ptr;
		this->size = size;
	}

	this->memory[ this->used++ ] = value;
	return DAE_NO_ERROR;
}

DynArrayError_t da_pop( DynArray_t* this, void** out ) {
	if (! this->size )  // is the array valid?
		return DAE_INVALID_ARRAY;

	if (! this->used )
		return DAE_EMPTY_ARRAY;

	this->used -= 1;

	if ( out != NULL )
		*out = this->memory[ this->used ];

	return DAE_NO_ERROR;
}

void da_sort( DynArray_t* this, Comparator_t comparator ) {
	if (! this->size )  // is the array valid?
		return;

	qsort( this->memory, this->used, sizeof( void* ), comparator );
}
void da_asort( DynArray_t* this, AComparator_t comparator, void* user ) {
	if (! this->size )  // is the array valid?
		return;

	asort( this->memory, this->used, sizeof( void* ), comparator, user );
}

typedef struct { size_t index; DynArray_t* obj; } IterState_t;
static nullable( const void* ) da_iter_next( void* iterator ) {
	IterState_t* state = iterator;
	if ( !state->obj || state->index >= state->obj->used )
		return NULL;

	return state->obj->memory[ state->index++ ];
}

static size_t da_iter_index( const void* iterator ) {
	return ( (IterState_t*) iterator )->index - 1;
}

DynArrayError_t da_iter_start( DynArray_t* this, Iterator_t* out ) {
	if (! this->size )  // is the array valid?
		return DAE_INVALID_ARRAY;

	IterState_t* state = malloc( sizeof( IterState_t ) );
	if ( !state )
		return DAE_ALLOC_FAILED;
	state->index = 0;
	state->obj = this;

	// Iterator_t is itself a pointer to a struct of two pointers, so doing sizeof( Iterator_t doesn't work )
	Iterator_t iterator = malloc( LIBAURA_ITERATOR_OBJECT_SIZE );
	if ( !iterator ) {
		free( state );
		return DAE_ALLOC_FAILED;
	}
	iterator->next = da_iter_next;
	iterator->index = da_iter_index;
	iterator->state = state;

	*out = iterator;
	return DAE_NO_ERROR;
}

nullable(Iterator_t) da_iter_create( DynArray_t* this ) {
	Iterator_t iter = NULL;
	da_iter_start( this, &iter );
	return iter;
}

DynArrayError_t da_iter_end( DynArray_t* this, Iterator_t* iterator ) {
	if (! this->size )  // is the array valid?
		return DAE_INVALID_ARRAY;

	IterState_t* state = ( *iterator )->state;
	// this is done, so you don't accidentally free the wrong iterator, which, yes, has happened to me many times
	if ( state->obj != this )
		return DAE_WRONG_THIS;

	free( state );
	free( *iterator );
	*iterator = NULL;
	return DAE_NO_ERROR;
}

void da_free( DynArray_t* this, DynArrayFreeFlags_t flags ) {
	// if asked, free the memory pointed by the pointers in the array
	if ( ( flags & DAFF_FREE_POINTERS ) != 0 )
		for ( int i = 0; i < this->used; i++ )
			free( this->memory[ i ] );

	free( this->memory );
	this->memory = NULL;
	this->size = 0;
	this->used = 0;
}

DynArrayError_t da_last( DynArray_t* this, void** out ) {
	if (! this->size )  // is the array valid?
		return DAE_INVALID_ARRAY;

	if ( this->used == 0 )
		return DAE_EMPTY_ARRAY;

	*out = this->memory[ this->used - 1 ];

	return DAE_NO_ERROR;
}

nullable(void*) da_last_n( DynArray_t* this ) {
	if (! this->size )  // is the array valid?
		return NULL;

	if ( this->used == 0 )
		return NULL;

	return this->memory[ this->used - 1 ];
}

void da_clear( DynArray_t* this ) {
	memset( this->memory, '\0', this->size * sizeof( void* ) );
	this->used = 0;
}

void da_ensure_clean( DynArray_t* this ) {
	memset( &this->memory[this->used + 1], '\0', ( this->size - this->used ) * sizeof( void* ) );
}
