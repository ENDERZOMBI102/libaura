//
// Created by Aurora on 26/04/2023.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "libaura/memory.h"
#include "libaura/string.h"

#include "libaura/ls_colors.h"


LsCodeError_t codeMap_new( const char* lsColors, LsCodeMap_t** out ) {
	size_t used = 0;
	size_t arrSize = 10;
	TRY_MALLOC_ARRAY( LsCodeEntry_t, arrSize, codes, return LCE_ALLOC_FAILED, "Failed to alloc LsCodeEntry_t array!" );

	size_t last = 0;
	size_t strSize = strlen( lsColors );
	for ( size_t i = 0; i < strSize; i += 1 ) {
		if ( lsColors[ i ] == '=' ) {
			codes[ used ].type = substr( lsColors, last, i - last );
			last = i + 1;
		} else if ( lsColors[ i ] == ':' ) {
			codes[ used ].code = substr( lsColors, last, i - last );
			used += 1;
			if ( used == arrSize ) {
				size_t newSize = ceil( (double) arrSize * 1.5 );
				auto ptr = reallocarray( codes, newSize, sizeof( LsCodeEntry_t ) );
				if ( !ptr )
					return LCE_REALLOC_FAILED;
				codes = ptr;
				arrSize = newSize;
			}
			i += 1;
			last = i;
		}
	}
	// try to only use as much space as we need, free the rest
	auto ptr = reallocarray( codes, used, sizeof( LsCodeEntry_t ) );
	// if we reallocated, good, else, we'll just use more memory than strictly necessary
	if ( ptr )
		codes = ptr;

	TRY_MALLOC_INTO( LsCodeMap_t, *out, return LCE_ALLOC_FAILED, "Failed to alloc LsCodeMap_t!" );
	memcpy( *out, &(LsCodeMap_t) { used, codes }, sizeof( LsCodeMap_t ) );

	return LCE_NO_ERROR;
}

void codeMap_free( LsCodeMap_t* this ) {
	for ( int i = 0; i < this->entryCount; ++i ) {
		free( (char*) this->entries[i].type );
		free( (char*) this->entries[i].code );
	}
	free( (LsCodeEntry_t*) this->entries );
}

nullable(const char*) codeMap_get( const LsCodeMap_t* this, const char* key ) {
	for ( int codeIndex = 0; codeIndex < this->entryCount; codeIndex++ )
		if ( strcmp( this->entries[ codeIndex ].type, key ) == 0 )
			return this->entries[ codeIndex ].code;

	return NULL;
}
