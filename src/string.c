//
// Created by Aurora on 15/04/2023.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libaura/memory.h"
#include "libaura/decorator.h"
#include "libaura/result.h"

#include "libaura/string.h"

Result( String_t, char ) x() {
	String_t string = new_from_raw( "Hello World!" );

	if (! string ) {
		Result_t res = OK( string );
		return res;
	} else {
		Result_t res = ERR( 1, "Failed to alloc string object" );
		return res;
	}
}

String_t new_from_raw( const char* raw ) {
	TRY_MALLOC( struct String, string, return NULL, "Failed to alloc string struct!!" );

	size_t len = strlen( raw );
	TRY_MALLOC_ARRAY_INTO( char, len, string->memory, return NULL, "Failed to alloc backing char array!" );
	// TODO: Finish this.
	return NULL;
}

StringError_t string_append( String_t this, char* other ) {

	return SE_ALLOC_FAILED;
}

StringError_t string_append_s( String_t this, String_t other ) {

	return SE_ALLOC_FAILED;
}

const char* string_get_raw( String_t this )  {
	struct String* self = this;
	return self->memory;
}

Slice(char) string_as_slice( String_t this ) {
	struct String* self = this;
	Slice_t result = { .ptr = self->memory, .size = self->size };
	return result;
}

nullable(const char*) substr( const char* this, const size_t offset, const size_t size ) {
	TRY_MALLOC_ARRAY( char, size + 1, tmp, return NULL, "Failed to alloc string subslice!" );
	memcpy( tmp, this + offset, size );
	tmp[size] = '\0';
	return tmp;
}
