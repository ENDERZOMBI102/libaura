//
// Created by Aurora on 24/10/2023.
//
#include <string.h>

#include "libaura/stringify.h"

char* btos( const bool value, char* out ) {
	if ( out == NULL )
		return value ? "true" : "false";
	return strcpy( out, value ? "true" : "false" );
}
