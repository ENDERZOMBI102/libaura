//
// Created by Firmament on 27/04/2023.
//
#include "libaura/sort.h"

#define __USE_GNU // NOLINT(bugprone-reserved-identifier)
#include <stdlib.h>


typedef struct { void* user; AComparator_t comparator; } SorterData_t;
static int asort__internal_callback( void* user, const void* a, const void* b ) {
	SorterData_t * data = (SorterData_t *) user;
	return ( data->comparator )( a, b, data->user );
}

void asort( void* base, size_t nmemb, size_t size, AComparator_t comparator, void* user ) {
	#if ( defined _GNU_SOURCE || defined __GNU__ || defined __linux__ )
		qsort_r( base, nmemb, size, comparator, user );
	#elif ( defined __APPLE__ || defined __MACH__ || defined __DARWIN__ )
		qsort_r( base, nmemb, size, &(SorterData_t) { user, comparator }, &asort__internal_callback );
	#elif ( defined _WIN32 || defined _WIN64 || defined __WINDOWS__ )
		qsort_s( *base, nmemb, size, &asort__internal_callback, &(SorterData_t) { user, comparator } );
	#else
		#error "Don't know what function to use for sorting!"
	#endif
}
