include_guard(GLOBAL)
enable_testing()

function( TEST )
	cmake_parse_arguments( PRM "" "FOR;REQUIRES" "" ${ARGN} )
	# parameter error checking
	if( DEFINED PRM_KEYWORDS_MISSING_VALUES )
		if( "FOR" IN_LIST PRM_KEYWORDS_MISSING_VALUES )
			message( SEND_ERROR "`TEST()`: Keyword `FOR` is missing required parameter." )
			return()
		elseif( "REQUIRES" IN_LIST PRM_KEYWORDS_MISSING_VALUES )
			message( SEND_ERROR "`TEST()`: Keyword `REQUIRES` is missing required parameter." )
			return()
		endif()
	endif()

	# if we were given a `REQUIRES` clause, check it
	if( DEFINED PRM_REQUIRES AND NOT ${${PRM_REQUIRES}} )
		message( NOTICE "Skipping adding tests for `${PRM_FOR}` as it depends on `${PRM_REQUIRES}`" )
		return()
	endif()

	# add the test
	add_executable( ${PRM_FOR} test/${PRM_FOR}.c )
	target_link_libraries( ${PRM_FOR} PRIVATE LibAura m )
	target_compile_definitions( ${PRM_FOR} PRIVATE "$<$<CONFIG:RELEASE>:RELEASE>" "$<$<CONFIG:DEBUG>:DEBUG>" )
	add_test( NAME ${PRM_FOR} COMMAND ${PRM_FOR} )
endfunction()

TEST( FOR ls_colors REQUIRES UNIX )
TEST( FOR sort )
TEST( FOR dbg )
TEST( FOR linkedlist )
TEST( FOR result )
TEST( FOR types )
TEST( FOR stringify )
