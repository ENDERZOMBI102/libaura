//
// Created by Aurora on 24/10/2023.
//
#pragma once

#pragma clang diagnostic push
#pragma ide diagnostic ignored "bugprone-reserved-identifier"

/*
 * COMPATABILITY TABLE:
 *   _BitInt     -> gcc: N/A, clang: 15 , MSVC: N/A
 *   true, false -> gcc: 13 , clang: 15 , MSVC: N/A
 *   bool        -> gcc: 13 , clang: 16 , MSVC: N/A
 *   nullptr     -> gcc: 13 , clang: 16 , MSVC: N/A
 *   auto        -> gcc: 13 , clang: N/A, MSVC: N/A // NO-IMPL
 */

#if defined( __clang_major__ )
	#if __clang_major__ < 16
		#define __NEED_BOOL_POLYFILL
		#define __NEED_NULLPTR_POLYFILL
	#endif
	#if __clang_major__ < 15
		#define __NEED_BITINT_POLYFILL
		#define __NEED_BOOLEANS_POLYFILL
	#endif
#elif defined( __GNUC__ )
	#define __NEED_BITINT_POLYFILL

	#if __GNUC__ < 13
		#define __NEED_BOOLEANS_POLYFILL
		#define __NEED_BOOL_POLYFILL
		#define __NEED_NULLPTR_POLYFILL
	#endif
#elif defined( _MSC_VER )
	#define __NEED_BITINT_POLYFILL
	#define __NEED_BOOLEANS_POLYFILL
	#define __NEED_BOOL_POLYFILL
	#define __NEED_NULLPTR_POLYFILL
#endif

#if defined( __NEED_BITINT_POLYFILL )
	#define _BitInt(...) _Pragma( "message(\"Compiler does not support _BitInt!!\")" )
	#undef __NEED_BITINT_POLYFILL
#endif
#if defined( __NEED_BOOLEANS_POLYFILL )
	#define true 1
	#define false 0
	#undef __NEED_BOOLEANS_POLYFILL
#endif
#if defined( __NEED_BOOL_POLYFILL )
	#define bool _Bool
	#undef __NEED_BOOL_POLYFILL
#endif
#if defined( __NEED_NULLPTR_POLYFILL )
	#define nullptr NULL
	#undef __NEED_NULLPTR_POLYFILL
#endif

#pragma clang diagnostic pop
