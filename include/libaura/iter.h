//
// Created by Aurora on 14/04/2023.
//

#pragma once
#include <stddef.h>

#include "libaura/decorator.h"
#include "libaura/oneline.h"

/**
 * Flag values to stop or continuing iterations.
 */
typedef enum IterResult { ITER_CONTINUE = 0, ITER_BREAK = 1 } IterResult_t;

/**
 * The callback for container-managed iteration.
 */
typedef IterResult_t(*IterCallback_t)( size_t index, const void** value, void* user );

/**
 * The size of a iterator object.
 */
#define LIBAURA_ITERATOR_OBJECT_SIZE ( sizeof( void* ) * 3 )

/**
 * A pointer to an iterator object, these objects should not be accessed normally (use the macros instead) unless you're implementing them for a container.<br/>
 * <b>Size</b> = 1 byte;
 */
typedef struct { nullable(const void*)(*next)( void* state ); size_t(*index)( const void* state ); void* state; }* Iterator_t;

/**
 * Advances the iterator and yields the next value.
 */
#define NEXT( iter ) iter->next( iter->state )

/**
 * The index of the last yielded value.
 */
#define INDEX( iter ) iter->index( iter->state )

/**
 * Iterates on an iterator, advancing it and setting $VARNAME to the current yielded value.
 */
#define FOR( type, varname, IN, iter ) \
    type varname;                      \
    while ( ( varname = (type) NEXT( iter ) ) )

/**
 * Iterates on a container which implements the iterator protocol.
 */
#define FOREACH( type, varname, IN, obj, prefix, body )     \
    auto _UNIQUE( iter_ ) = prefix ## _iter_create( &obj ); \
    FOR( type, varname, IN, _UNIQUE( iter_ ) ) body         \
	prefix ## _iter_end( &obj, &_UNIQUE( iter_ ) );
