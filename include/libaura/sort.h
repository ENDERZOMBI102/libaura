//
// Created by Firmaments on 27/04/2023.
//

#pragma once
#include <stddef.h>
#include <stdint.h>

#include "libaura/decorator.h"


/**
 * A comparator function for sorting a container.
 * @param a first element.
 * @param b second element.
 * @returns -1 if the first argument is greater <br/>
 * 			 0 if they're equal <br/>
 * 			 1 if the second one is greater.
 */
typedef range(int32_t, -1, 1) ( *Comparator_t )( const void* a, const void* b );

/**
 * A comparator function for sorting a container.
 * @param a first element.
 * @param b second element.
 * @param user user parameter given to `asort`, may contain state.
 * @returns -1 if the first argument is greater <br/>
 * 			 0 if they're equal <br/>
 * 			 1 if the second one is greater.
 */
typedef range(int32_t, -1, 1) ( *AComparator_t )( const void* a, const void* b, void* user );

/**
 * Sorts the $nmemb elements of the array pointed to by $base, each element $size bytes long, using the $comparator function to determine the order.<br/>
 * This function also accepts an additional $user parameter to give to the comparator function, which may be used as state.<br/>
 * This function is an abstraction over the platform's native `qsort_r` function, and so only used as an adapter.
 * @param base the base of the array to sort.
 * @param nmemb number of elements to sort.
 * @param size size of an element.
 * @param comparator comparator function to use.
 * @param user user parameter to pass to the comparator function.
 */
void asort( void* base, size_t nmemb, size_t size, AComparator_t comparator, void* user );
