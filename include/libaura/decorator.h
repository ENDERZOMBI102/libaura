//
// Created by Firmament on 19/04/2023.
//
#pragma once
#pragma clang diagnostic push
	#pragma ide diagnostic ignored "bugprone-reserved-identifier"

	/**
	 * Tells the possible ranges of a value.
	 */
	#define range( type, min, max ) type

	/**
	 * This might be NULL.
	 */
	#define nullable( type ) type

	/**
	 * This can't be NULL.
	 */
	#define notnull( type ) type

	/**
	 * Automatically deduce type of variable.
	 */
	#define auto __auto_type

	/**
	 * Box, passing means moving.
	 */
	#define Box(inner) inner*

	/**
	 * Shared, passing means copying.
	 */
	#define Shared(inner) inner*

	/**
	 * Defined that a parameter is an INPUT pointer
	 */
	#define IN

	/**
	 * Defined that a parameter is both an INPUT and an OUTPUT pointer,
	 * depending on context
	 */
	#define INOUT

	/**
	 * Defined that a parameter is an OUTPUT pointer
	 */
	#define OUT

#pragma clang diagnostic pop