//
// Created by Aurora on 22/05/2023.
//
#pragma once


/**
 * Small macro used to make it clear that a value is returned from a expression block like @code{.c++} ({ 0; }) @endcode.
 */
#define yield(x) x

/**
 * Printf + fflush(stdout)
 */
#define flprintf( args... ) printf( args ); fflush( stdout )

#define _JOIN__( prefix, suffix ) prefix ## suffix
/**
 * Joins two tokens.
 */
#define _JOIN( prefix, suffix ) _JOIN__( prefix, suffix )

#if !defined( _STRINGIFY )
	#define _STRINGIFY__( x ) # x
	/**
	 * Makes the given token a string.
	 */
	#define _STRINGIFY( x ) _STRINGIFY__( x )
#endif

/**
 * Creates a unique version of a given name,
 * watch out, names are line-defined!
 */
#define _UNIQUE(name) _JOIN( name, __LINE__ )

