//
// Created by Aurora on 21/04/2023.
//
#pragma once

#include <stdlib.h>

#include "libaura/polyfill.h"
#include "libaura/iter.h"
#include "libaura/error.h"
#include "libaura/result.h"
#include "libaura/sort.h"

/**
 * A linked list.<br/>
 * <b>Size</b> = 24 bytes
 */
typedef struct { size_t size; struct Node* node; struct Node* last; } LinkedList_t;
/**
 * An error for LinkedList operations.
 */
typedef enum { LLE_NO_ERROR = 0, LLE_ALLOC_FAILED, LLE_INDEX_OUT_OF_BOUNDS, LLE_WRONG_THIS, LLE_EMPTY_ARRAY, LLE_INVALID_ARRAY } LinkedListError_t;
/**
 * Flags for the free method.
 */
typedef enum { LLFF_DONT_FREE_POINTERS = 0, LLFF_FREE_POINTERS } LinkedListFreeFlags_t;


/**
 * Gets the object at a given index.
 * @param this the LinkedList instance.
 * @param index index to get.
 * @param out where to put the get'd object.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_INDEX_OUT_OF_BOUNDS.
 */
Result(void*, LinkedListError_t) ll_get( LinkedList_t* this, size_t index );

/**
 * Iterates over a dynamic array, calling a callback over each element.
 * @param this the LinkedList instance.
 * @param callback the function to call over the elements of the array.
 * @return true if we stopped earlier, false otherwise.
 */
bool ll_iter( const LinkedList_t* this, IterCallback_t callback, void* user );

/**
 * Sets the name of a particular index of the array.
 * @param this the LinkedList instance.
 * @param index the index to set.
 * @param name name to set the index to.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_INDEX_OUT_OF_BOUNDS.
 */
LinkedListError_t ll_set( LinkedList_t* this, size_t index, void* value );

/**
 * Removes the name at index, and moves the rest of the array back one position.
 * @param this the LinkedList instance.
 * @param index index of the name to remove.
 * @param out where to put the removed name.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_INDEX_OUT_OF_BOUNDS.
 */
LinkedListError_t ll_remove( LinkedList_t* this, size_t index, void** out );

/**
 * Pushes a name to the end of the array.
 * @param this the LinkedList instance.
 * @param name name to push.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_ALLOC_FAILED.
 */
LinkedListError_t ll_push( LinkedList_t* this, void* value );

/**
 * Pops the name at the end of the array.
 * @param this the LinkedList instance.
 * @param out where to put the pop'd name.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_EMPTY_ARRAY.
 */
LinkedListError_t ll_pop( LinkedList_t* this, void** out );


/**
 * Sorts the array using qsort.
 * @param this the LinkedList instance.
 * @param comparator comparator function to use, -1 == first is bigger, 0 == equal, 1 == second is bigger.
 */
void ll_sort( LinkedList_t* this, Comparator_t comparator );

/**
 * Sorts the array using `asort`, an abstraction over `qsort_r` from different vendors.
 * @param this the LinkedList instance.
 * @param comparator comparator function to use, -1 == first is bigger, 0 == equal, 1 == second is bigger.
 * @param user objet to pass to the comparator function.
 */
void ll_asort( LinkedList_t* this, AComparator_t comparator, void* user );

/**
 * Initializes an iterator object for iteration in this dynamic array.
 * @param this the LinkedList instance.
 * @param out where to put the iterator object.
 * @returns either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_ALLOC_FAILED.
 */
LinkedListError_t ll_iter_start( LinkedList_t* this, Iterator_t* out );

/**
 * Creates an iterator object for this dynamic array.
 * @param this the LinkedList instance.
 * @returns either NULL or an Iterator_t instance pointer.
 */
nullable(Iterator_t) ll_iter_create( LinkedList_t* this );

/**
 * Deconstructs and NULLs an iterator object. <br/>
 * Objects must have been created from `this` dynamic array.
 * @param this the LinkedList instance.
 * @param iterator the iterator object to destroy.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_WRONG_THIS.
 */
LinkedListError_t ll_iter_end( LinkedList_t* this,  Iterator_t* iterator );

/**
 * Frees `this` dynamic array's memory and resets it to zero-ed state.
 * @param this the LinkedList instance.
 * @param flags flags used to determine the behavior of `ll_free`.
 */
void ll_free( LinkedList_t* this, LinkedListFreeFlags_t flags );

/**
 * Sets out to the last value in the array.
 * @param this the LinkedList instance.
 * @param out where to put the get'd value.
 * @returns either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_EMPTY_ARRAY.
 */
LinkedListError_t ll_last( LinkedList_t* this, void** out );

/**
 * Clears `this` dynamic array, maintaining its size.
 * @param this the LinkedList instance.
 */
void ll_clear( LinkedList_t* this );

/**
 * Makes sure every free slot after the last used one is `NULL`.
 * @param this the LinkedList instance.
 */
void ll_ensure_clean( LinkedList_t* this );
