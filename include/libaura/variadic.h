//
// Created by Aurora on 15/04/2023.
//

#pragma once

/**
 * Declares and initialize the va_list C construct.
 */
#define VARGS( varname, lastParam ) \
	va_list varname;                \
	va_start( varname, lastParam )