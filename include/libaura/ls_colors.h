//
// Created by Aurora on 26/04/2023.
//

#pragma once


#include "libaura/decorator.h"
#include <stdint.h>
#include <stddef.h>

/**
 * Represent an entry in the code table.<br/>
 * <b>Size</b> = 2 bytes
 */
typedef struct { const char* type; const char* code; } LsCodeEntry_t;
/**
 * A map representing the parsed LS_COLORS environment variable.<br/>
 * <b>Size</b> = 8 bytes
 */
typedef struct { const size_t entryCount; const LsCodeEntry_t* entries; } LsCodeMap_t;
/**
 * Error enum.<br/>
 * <b>Size</b> = 1 byte
 */
typedef enum { LCE_NO_ERROR = 0, LCE_ALLOC_FAILED, LCE_REALLOC_FAILED } LsCodeError_t;

/**
 * Parses a string formatted like the `LS_COLORS` environment variable.
 * @param lsColors a LS_COLORS-formatted string.
 * @param out where to put the parsed LsCodeMap
 * @returns either LCE_NO_ERROR or LCE_ALLOC_FAILED
 */
LsCodeError_t codeMap_new( const char* lsColors, LsCodeMap_t** out );

/**
 * Frees a code map.
 * @param this the CodeMap instance.
 */
void codeMap_free( LsCodeMap_t* this );

/**
 * Finds an entry in the map, and returns it's code.
 * @param this the LsCodeMap_t instance.
 * @param key entry type to find.
 * @return the entry's code, or NULL.
 */
nullable(const char*) codeMap_get( const LsCodeMap_t* this, const char* key );
