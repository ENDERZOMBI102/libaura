//
// Created by Aurora on 14/04/2023.
//

#pragma once

#include <stdlib.h>

#include "libaura/polyfill.h"
#include "libaura/attribute.h"
#include "libaura/iter.h"
#include "libaura/sort.h"

/**
 * A dynamic array, which can grow and shrink in size.<br/>
 * Size = 24 bytes
 */
typedef struct DynArray { size_t size; size_t used; void** memory; } DynArray_t;
/**
 * An error for DynArray operations.
 */
typedef enum { DAE_NO_ERROR = 0, DAE_ALLOC_FAILED, DAE_INDEX_OUT_OF_BOUNDS, DAE_WRONG_THIS, DAE_EMPTY_ARRAY, DAE_INVALID_ARRAY } DynArrayError_t;
/**
 * An error for DynArray operations.
 */
typedef enum { DAFF_DONT_FREE_POINTERS = 0, DAFF_FREE_POINTERS } DynArrayFreeFlags_t;


/**
 * Constructs a new Dynamic Array with a given initial size.
 * @param out where the dynamic array will be put.
 * @param initialSize the initial size of the dynamic array.
 * @return either DAE_NO_ERROR or DAE_ALLOC_FAILED.
 */
DynArrayError_t da_new( DynArray_t* out, size_t initialSize );

/**
 * Gets the object at a given index.
 * @param this the DynArray instance.
 * @param index index to get.
 * @param out where to put the get'd object.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_INDEX_OUT_OF_BOUNDS.
 */
DynArrayError_t da_get( const DynArray_t* this, size_t index, void** out );

/**
 * Iterates over a dynamic array, calling a callback over each element.
 * @param this the DynArray instance.
 * @param callback the function to call over the elements of the array.
 * @return true if we stopped earlier, false otherwise.
 */
bool da_iter( const DynArray_t* this, IterCallback_t callback, void* user );

/**
 * Sets the name of a particular index of the array.
 * @param this the DynArray instance.
 * @param index the index to set.
 * @param name name to set the index to.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_INDEX_OUT_OF_BOUNDS.
 */
DynArrayError_t da_set( DynArray_t* this, size_t index, void* value );

/**
 * Removes the name at index, and moves the rest of the array back one position.
 * @param this the DynArray instance.
 * @param index index of the name to remove.
 * @param out where to put the removed name.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_INDEX_OUT_OF_BOUNDS.
 */
DynArrayError_t da_remove( DynArray_t* this, size_t index, void** out );

/**
 * Pushes a name to the end of the array.
 * @param this the DynArray instance.
 * @param name name to push.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_ALLOC_FAILED.
 */
DynArrayError_t da_push( DynArray_t* this, void* value );

/**
 * Pops the name at the end of the array.
 * @param this the DynArray instance.
 * @param out where to put the pop'd name.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_EMPTY_ARRAY.
 */
DynArrayError_t da_pop( DynArray_t* this, void** out );


/**
 * Sorts the array using qsort.
 * @param this the DynArray instance.
 * @param comparator comparator function to use, -1 == first is bigger, 0 == equal, 1 == second is bigger.
 */
void da_sort( DynArray_t* this, Comparator_t comparator );

/**
 * Sorts the array using `asort`, an abstraction over `qsort_r` from different vendors.
 * @param this the DynArray instance.
 * @param comparator comparator function to use, -1 == first is bigger, 0 == equal, 1 == second is bigger.
 * @param user objet to pass to the comparator function.
 */
void da_asort( DynArray_t* this, AComparator_t comparator, void* user );

/**
 * Initializes an iterator object for iteration in this dynamic array.
 * @param this the DynArray instance.
 * @param out where to put the iterator object.
 * @returns either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_ALLOC_FAILED.
 */
DynArrayError_t da_iter_start( DynArray_t* this, Iterator_t* out );

/**
 * Creates an iterator object for this dynamic array.
 * @param this the DynArray instance.
 * @returns either NULL or an Iterator_t instance pointer.
 */
nullable(Iterator_t) da_iter_create( DynArray_t* this );

/**
 * Deconstructs and NULLs an iterator object. <br/>
 * Objects must have been created from `this` dynamic array.
 * @param this the DynArray instance.
 * @param iterator the iterator object to destroy.
 * @return either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_WRONG_THIS.
 */
DynArrayError_t da_iter_end( DynArray_t* this,  Iterator_t* iterator );

/**
 * Frees `this` dynamic array's memory and resets it to zero-ed state.
 * @param this the DynArray instance.
 * @param flags flags used to determine the behavior of `da_free`.
 */
void da_free( DynArray_t* this, DynArrayFreeFlags_t flags );

/**
 * Sets out to the last value in the array.
 * @param this the DynArray instance.
 * @param out where to put the get'd value.
 * @returns either DAE_NO_ERROR, DAE_INVALID_ARRAY or DAE_EMPTY_ARRAY.
 */
DynArrayError_t da_last( DynArray_t* this, void** out );

/**
 * Returns the (possibly NULL) last item in this array.
 * @param this the DynArray instance.
 * @returns either the element or NULL.
 */
nullable(void*) da_last_n( DynArray_t* this );

/**
 * Clears `this` dynamic array, maintaining its size.
 * @param this the DynArray instance.
 */
void da_clear( DynArray_t* this );

/**
 * Makes sure every free slot after the last used one is `NULL`.
 * @param this the DynArray instance.
 */
void da_ensure_clean( DynArray_t* this );
