//
// Created by Firmament on 18/04/2023.
//

#pragma once
#include "libaura/error.h"

/**
 * Tries to malloc a type, and if it fails, errors out.
 */
#define TRY_MALLOC( type, varname, close, message... ) \
	type* varname = malloc( sizeof( type ) );          \
	TRY( varname, close, message )

/**
 * Tries to malloc a type into a variable, and if it fails, errors out.
 */
#define TRY_MALLOC_INTO( type, variable, close, message... ) \
	variable = malloc( sizeof( type ) );                     \
	TRY( variable, close, message )

/**
 * Tries to malloc an array of type with length size, if it fails, errors out.
 */
#define TRY_MALLOC_ARRAY( type, size, varname, close, message... ) \
	type* varname = calloc( size, sizeof( type ) );                \
	TRY( varname, close, message )

/**
 * Tries to malloc an array of type with length size into variable, if it fails, errors out.
 */
#define TRY_MALLOC_ARRAY_INTO( type, size, variable, close, message... ) \
	variable = calloc( size, sizeof( type ) );                           \
	TRY( variable, close, message )
