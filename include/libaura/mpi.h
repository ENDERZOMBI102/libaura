//
// Created by Aurora on 24/10/2023.
//
#pragma once

/**
 * Initializes MPI and loads the world size and the process's rank into variables
 */
#define MPIA_Init( rankVarName, sizeVarName, initArgs... ) \
	MPI_Init( initArgs );                                  \
	i32 rankVarName = 0;                                   \
	i32 sizeVarName = 0;                                   \
	MPI_Comm_rank( MPI_COMM_WORLD, &rankVarName );         \
	MPI_Comm_size( MPI_COMM_WORLD, &sizeVarName )

/**
 * Checks whether a rank is considered the master
 */
#define MPIA_IsMain( who ) !who

/**
 * Allocates a new window of the specified type and array size, into the output,
 * and making a view of the buffer available inside buffer.
 */
#define MPIA_WinAlloc( type, size, buffer, output ) MPI_Win_allocate( (MPI_Aint) sizeof(type) * (size), sizeof(type), MPI_INFO_NULL, MPI_COMM_WORLD, buffer, output )
