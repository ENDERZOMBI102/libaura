//
// Created by Aurora on 17/04/2023.
//

#pragma once
#include <stdlib.h>

/**
 * A pointer with an associated total.
 * <b>Size</b> = 16 bytes
 */
typedef struct { const void* ptr; size_t size; } Slice_t;

/**
 * A slice of type $TYPE.
 */
#define Slice(type, size...) Slice_t
