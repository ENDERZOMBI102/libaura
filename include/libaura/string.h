//
// Created by Aurora on 15/04/2023.
//

#pragma once
#include <stdlib.h>

#include "libaura/decorator.h"
#include "libaura/attribute.h"
#include "libaura/slice.h"

struct String { size_t total; size_t size; char* memory; };
typedef void* String_t;
typedef enum { SE_NO_ERROR = 0, SE_ALLOC_FAILED, SE_INDEX_OUT_OF_BOUNDS } StringError_t;

String_t new_from_raw( const char* raw );
StringError_t string_append( String_t this, char* other );
StringError_t string_append_s( String_t this, String_t other );
const char* string_get_raw( String_t this );
Slice(char) string_as_slice( String_t this );

/**
 * Allocates a substring of which contents are from $this + $offset with and size $size
 * @param this the string to take the data from.
 * @param offset where to start to copy from.
 * @param size size of the segment to copy.
 * @return a new malloc'd string.
 */
nullable(const char*) substr( const char* this, size_t offset, size_t size );
