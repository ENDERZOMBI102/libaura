//
// Created by Aurora on 24/10/2023.
//
#pragma once

#include "libaura/polyfill.h"



/**
 * Converts a given boolean value to string, and inserts it in the provided buffer,
 * which must be of at least 5 characters + null terminator ( 6 in total ).
 *
 * If the provided buffer is `NULL`, a static string is returned, else, the provided buffer.
 */
char* btos( bool value, char* out );
