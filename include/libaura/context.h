//
// Created by Firmament on 19/04/2023.
//

#pragma once

/**
 * Used to manage an object's lifetime, when the scope is left, the variable will be destroyed using the given closer function and then NULL'd out.
 */
#define WITH( type, varname, initializer, closer ) \
	for ( type varname initializer; varname; ( closer( varname ), varname = NULL ) )
