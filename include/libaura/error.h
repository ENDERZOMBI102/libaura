//
// Created by Aurora on 15/04/2023.
//

#pragma once

/**
 * Checks if an expr has a falsy value, and if it does, errors out.
 */
#define TRY( expr, close, message... ) \
	if ( !( expr ) ) {                 \
        extern FILE* errStream;        \
		fprintf( errStream, message ); \
		close;                         \
	} 0

#ifdef DEBUG
	/**
 	 * Like printf, but only prints when compiling in the DEBUG configuration
 	 */
	#define DBG( fmt, args... ) {                                                                  \
            extern int printf( const char* __restrict __format, ... );                             \
			printf( "%s@" __FILE_NAME__ " line " _STRINGIFY(__LINE__) ": " fmt, __func__, args ); \
		}0
#else
	#define DBG( args... ) 0
#endif

