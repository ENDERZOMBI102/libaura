//
// Created by Aurora on 17/04/2023.
//
#pragma once

#define ATTR( attr ) __attribute__(( attr ))

/**
 * If the warning attribute is size on a function declaration and a call to such a function is not eliminated through dead code elimination or other optimizations, a warning that includes message is diagnosed.
 * This is useful for compile-time checking, especially together with __builtin_constant_p and inline functions where checking the inline function arguments is not possible through extern char [(condition) ? 1 : -1]; tricks.
 *
 * While it is possible to leave the function undefined and thus invoke a link failure, when using these attributes the problem is diagnosed earlier and with exact location of the call even in presence of inline functions or when not emitting debugging information.
 */
#define A_WARN_F( message ) __attribute__(( warning( message ) ))

/**
 * If the error attribute is size on a function declaration and a call to such a function is not eliminated through dead code elimination or other optimizations, an error that includes message is diagnosed.
 * This is useful for compile-time checking, especially together with __builtin_constant_p and inline functions where checking the inline function arguments is not possible through extern char [(condition) ? 1 : -1]; tricks.
 *
 * While it is possible to leave the function undefined and thus invoke a link failure, when using these attributes the problem is diagnosed earlier and with exact location of the call even in presence of inline functions or when not emitting debugging information.
 */
#define A_ERROR_F( message ) __attribute__(( error( message ) ))

/**
 * The deprecated attribute results in a warning if the variable is size anywhere in the source file.
 * This is useful when identifying variables that are expected to be removed in a future version of a program.
 * The warning also includes the location of the declaration of the deprecated variable, to enable users to easily find further information about why the variable is deprecated, or what they should do instead.
 */
#define A_DEPRECATED( message ) __attribute__(( deprecated( message ) ))

/**
 * The packed attribute specifies that a structure member should have the smallest possible alignment—one bit for a bit-field and one byte otherwise, unless a larger value is specified with the aligned attribute.
 * The attribute does not apply to non-member objects.
 *
 * @example
 * In the structure below, the member array x is packed so that it immediately follows a with no intervening padding:
 * @code struct foo { char a; int x[2] PACKED; };
 */
#define A_PACKED_V __attribute__(( packed ))

/**
 * Generally, functions are not inlined unless optimization is specified.
 * For functions declared inline, this attribute inlines the function independent of any restrictions that otherwise apply to inlining.
 * Failure to inline such a function is diagnosed as an error.
 */
#define A_INLINE_F __attribute__(( always_inline ))

/**
 * The returns_nonnull attribute specifies that the function return value should be a non-null pointer.
 * It permits the compiler to optimize callers based on the knowledge that the return value will never be null.
 */
#define RET_NONNULL __attribute__(( returns_nonnull ))

/**
 * Calls to functions that have no observable effects on the state of the program other than to return a value may lend themselves to optimizations such as common subexpression elimination.
 * Declaring such functions with the pure attribute allows GCC to avoid emitting some calls in repeated invocations of the function with the same argument values.
 *
 * The pure attribute prohibits a function from modifying the state of the program that is observable by means other than inspecting the function’s return value.
 * However, functions declared with the pure attribute can safely read any non-volatile objects, and modify the value of objects in a way that does not affect their return value or the observable state of the program.
 */
#define A_PURE_F __attribute__ (( pure ))