//
// Created by ENDERZOMBI102 on 04/07/2023.
//

#pragma once

#include "libaura/oneline.h"
#include "libaura/decorator.h"


/**
 * Represents either a Result or an error.
 * <b>Size</b> = 24 bytes
 */
typedef struct { void* value; long errorCode; } Result_t;

/**
 * A Result with value of type $TYPE and error type $ERROR
 */
#define Result( type, error ) Result_t

/**
 * Constructs a Result_t object in an error state with the given code and message.
 */
#define ERR( code, message ) _Generic( message, char*: (Result_t) { message, code } )

/**
 * Constructs a Result_t object in a ok state with the given value.
 */
#define OK( expr ) (Result_t) { expr, -1 }

/**
 * Constructs a Result_t object in a ok state with the given value.
 */
#define IS_OK( result ) !result.errorCode

/**
 * Tries to execute the given `Result_t`-returning expression, executing the catch block if an error occurs.<br>
 * Currently @code{.c}_Generic( typeof((block)), void: ( (block), NULL ), else: (block) );@endcode does not work, a paper has been submitted to the WG14 to add support,<br/>
 * and on the LLVM forums there were feature and pull requests to add support for this, obviously i've added my vote to it as it is required for this beloved mess to work.
 */
#define TRY_RUN( T, expr, OR_CATCH, E, AS, eName, block )   \
	({                                                      \
		Result_t _UNIQUE( res ) = expr;                     \
		T finalRes;                                         \
		if ( _UNIQUE( res ).errorCode != -1 ) {             \
			E eName = (E) _UNIQUE( res ).errorCode;         \
			char* message = _UNIQUE( res ).value;           \
			finalRes = (T) (block);                         \
		} else                                              \
			finalRes = (T) _UNIQUE( res ).value;            \
		finalRes;                                           \
	})
