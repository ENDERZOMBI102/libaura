//
// Created by Aurora on 08/10/2023.
//
#pragma once

// integer types
typedef char i8;
typedef unsigned char u8;

typedef short i16;
typedef unsigned short u16;

typedef int i32;
typedef unsigned int u32;

typedef long i64;
typedef unsigned long u64;

typedef long i64;
typedef unsigned long u64;

typedef __int128 i128;
typedef unsigned __int128 u128;

#if defined( _LP64 )
	typedef long isize;
	typedef unsigned long usize;
#else
	typedef int isize;
	typedef unsigned int usize;
#endif

// floating point types
typedef float f32;
typedef double f64;
typedef long double f128;

// string ptr
typedef char* str;
