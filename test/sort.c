//
// Created by Firmament on 27/04/2023.
//
#include <assert.h>
#include <string.h>

#include "libaura/sort.h"

range(int32_t, -1, 1) sorter( const void* ap, const void* bp, void* user ) {
	int a = *( (int*) ap );
	int b = *( (int*) bp );
	assert( user == NULL );
	return a > b ? 1 : ( a == b ? 0 : -1 );
}

int main() {
	#define COUNT 10
	int inc[ COUNT ] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, };
	int tmp[ COUNT ] = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, };

	asort( tmp, COUNT, sizeof( int ), sorter, NULL );
	assert( memcmp( tmp, inc, COUNT * sizeof( int ) ) == 0 );
}
