//
// Created by ENDERZOMBI102 on 04/07/2023.
//

#include "libaura/result.h"
#include "libaura/decorator.h"
#include "libaura/oneline.h"
#include <assert.h>
#include <string.h>

Result( char*, int ) stringValid() {
	return OK( "Everything was ok" );
}

Result( char*, int ) stringError() {
	return ERR( 12, "Error 12" );
}

int main() {
	auto data = TRY_RUN( char*, stringValid(), OR CATCH, int, AS, eCode, ({ yield(message); }));
	assert( strcmp( data, "Everything was ok" ) == 0 );

	data = TRY_RUN( char*, stringError(), OR CATCH, int, AS, eCode, ({ yield(message); }));
	assert( strcmp( data, "Error 12" ) == 0 );
}
