//
// Created by Aurora on 22/05/2023.
//
#include <stdio.h>
#include <string.h>

#include "libaura/oneline.h"

#include "libaura/linkedlist.h"

FILE* errStream;
int main() {
	errStream = stderr;

	LinkedList_t list = { };
	for ( size_t i = 10; i <= 20; i++ )
		ll_push( &list, (void*) i );
	printf( "size: %zu\n", list.size );

	auto x = TRY_RUN( size_t, ll_get( &list, 8 ), OR CATCH, LinkedListError_t, error, ({
		puts( message );
		yield( strlen( message ) );
	}));
	printf( "x: %zu\n", x );

	auto y = TRY_RUN( size_t, ll_get( &list, 12 ), OR CATCH, LinkedListError_t, error, ({
		puts( message );
		yield( strlen( message ) );
	}));
	printf( "y: %zu\n", y );
}