//
// Created by Aurora on 24/10/2023.
//
#include "libaura/stringify.h"
#include <assert.h>
#include <string.h>

int main() {
	assert( strcmp( btos( true, NULL ), "true" ) == 0 );
	assert( strcmp( btos( false, NULL ), "false" ) == 0 );
	char buffer[6] = { 0 };
	assert( strcmp( btos( true, buffer ), "true" ) == 0 );
	assert( strcmp( btos( false, buffer ), "false" ) == 0 );
}
