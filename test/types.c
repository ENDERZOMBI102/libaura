//
// Created by Aurora on 08/10/2023.
//
#include <assert.h>

#include "libaura/types.h"


int main() {
	// integers
	static_assert( sizeof( usize ) == 64 / 8 );
	static_assert( sizeof( isize ) == 64 / 8 );
	static_assert( sizeof( u128 ) == 128 / 8 );
	static_assert( sizeof( i128 ) == 128 / 8 );
	static_assert( sizeof( u64 ) == 64 / 8 );
	static_assert( sizeof( i64 ) == 64 / 8 );
	static_assert( sizeof( u32 ) == 32 / 8 );
	static_assert( sizeof( i32 ) == 32 / 8 );
	static_assert( sizeof( u16 ) == 16 / 8 );
	static_assert( sizeof( i16 ) == 16 / 8 );
	static_assert( sizeof( u8 ) == 8 / 8 );
	static_assert( sizeof( i8 ) == 8 / 8 );
	// floating
	static_assert( sizeof( f32 ) == 32 / 8 );
	static_assert( sizeof( f64 ) == 64 / 8 );
	static_assert( sizeof( f128 ) == 128 / 8 );
}
