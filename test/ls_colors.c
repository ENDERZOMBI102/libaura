//
// Created by Aurora on 26/04/2023.
//

#include "libaura/ls_colors.h"
#include "libaura/decorator.h"
#include <assert.h>
#include <stdlib.h>

int main() {
	LsCodeMap_t* map;
	assert( codeMap_new( getenv( "LS_COLORS" ), &map ) == LCE_NO_ERROR );

	for ( auto i = 0; i < map->entryCount; i += 1 ) {
		assert( map->entries[ i ].type[0] != '\0' );
		assert( map->entries[ i ].code[0] != '\0' );
	}

	codeMap_free( map );
	free( map );
}
